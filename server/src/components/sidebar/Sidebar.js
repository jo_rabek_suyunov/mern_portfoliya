import axios from 'axios';
import {Link} from 'react-router-dom';
import { useState, useEffect } from 'react';
import "./sidebar.css";

function Sidebar() {
    const [cats, setCats] = useState([]);
    useEffect(()=>{
        const getCats = async () =>{
            const res = await axios.get("/category");
            setCats(res.data)

        }

        getCats()
    }, [])
    return (
        <div className={'sidebar'}>
            <div className={"sidebarItem"}>
                <span className="sidebarTitle">ABOUT ME</span>
                <img
                    width={'100%'}
                    src={"https://wallpapershome.ru/images/pages/ico_h/23407.jpeg"}
                    alt={""}
                />
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    At laborum magnam magni maxime placeat praesentium qui quod sequi similique soluta?
                    Alias dolorem maxime officia quas vel, veniam voluptates? Obcaecati, ratione.
                </p>
            </div>
            <div className={"sidebarItem"}>
                <span className={'sidebarTitle'}>CATEGORIES</span>
                <ul className={'sidebarList'}>
                    {cats.map(c=>(
                       <Link to={`/?cat=${c.name}`}>
                           <li className="sidebarListItem">{c.name}</li>
                       </Link>
                    ))}
                </ul>
            </div>
            <div className={'sidebarItem'}>
                <span className={'sidebarTitle'}>FOLLOW US</span>
                <div className={'sidebarSocial'}>
                    <i className={"sidebarIcon fab fa-facebook-square"}></i>
                    <i className={"sidebarIcon fab fa-twitter-square"}></i>
                    <i className={"sidebarIcon fab fa-pinterest-square"}></i>
                    <i className={"sidebarIcon fab fa-instagram-square"}></i>
                </div>

            </div>

            </div>
    );
}

export default Sidebar;