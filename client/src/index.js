import ReactDom from 'react-dom'
import App from "./App";
import 'bootstrap/dist/css/bootstrap.min.css';
import { ContextProvider } from './context/Context';


ReactDom.render(
    <ContextProvider>
        <App/>
    </ContextProvider>
    ,document.getElementById('root'))