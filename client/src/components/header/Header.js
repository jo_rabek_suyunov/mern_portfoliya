import './header.css';

function Header() {
    return (
        <div className={'header'}>
            <div className="headerTitles">
                <span className="headerTitleSm">React & Node </span>
                <span className="headerTitleLg">Blog</span>
            </div>
            <img
                className={'headerImg'}
                src="https://wallpapershome.ru/images/pages/ico_h/22910.jpeg"
                alt=""
            />
        </div>
    );
}

export default Header;